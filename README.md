# GitLab Manager

---

## Introduction

This is a tiny tool, written in Python, to ease GitLab server administration from GitLab API

The tool is based in [*python-gitlab*](https://github.com/python-gitlab/python-gitlab) module, offering a set of functions to ease simple and common tasks with calls to the [*python-gitlab* API](https://python-gitlab.readthedocs.io) 

## Requirements

This tool has some dependencies:
	
- **Python 3**
	+ pip
	+ python-gitlab
	+ requests
- **Network connection**

## Installation


### Download sources

To download sources, we can clone the repository using Git

	git clone https://gitlab.com/AlmuHS/gitlab_manager.git
	
Enter in the directory to use the scripts:

	cd gitlab_manager/

### Installing pip

- Installation in Debian GNU/Linux
	
		apt install python3-pip

### Installing dependencies

	pip3 install -r requirements.txt
	

## Usage

### Generate a private token

To access to GitLab via API, It's necessary to generate a private token

This can be generated from user settings, in GitLab web interface

- **Step 1**: Enter to user settings, in GitLab

	![open_settings](images/settings.png)

- **Step 2**: In settings menu, select in "Access Tokens"

	- Select *api* scope.

		![generate_token](images/generate_token.png)

	- Set a name (and expiration date, if you want)
	
		![generate_token2](images/generate_token2.png)
		
- **Step 3**: Generate token

	- Press in "Create personal access token"
		
		![generate_token2](images/generate_token2.png)
		
	- Get the token string
	
		![get_token](images/get_token.png)
		
		Copy the token string to a file. 
	
		**THIS CANNOT BE RECOVERED AFTER THE WINDOW WILL CLOSED.  COPY IT IN A SECURE PLACE**
	
	 	We will use It to Sign In in GitLab in the tool
		
###  Sign In in GitLab using private token

To sign in to GitLab in GitLab_Manager, we can use `gitlab_login(url, priv_token)` function

The syntax is:

	log_in = login.GitLabLogin.login(url_of_gitlab_server, token_string)
	
By example:

	log_in = login.GitLabLogin.login('https://gitlab.com', 'aBcdeFG')
	
This function will return an object (`log_in` in the example), which will allows to access to the another classes of GitLab Manager


### Sign In in GitLab from config file

To avoid expose data in plain text in the script, we can store the URL and private token in a config file, based in this [example](https://python-gitlab.readthedocs.io/en/stable/cli.html#content)

We need to create a config file with this format:

	[global]
	default = somewhere
	ssl_verify = true
	timeout = 5
	
	[somewhere]
	url = https://some.whe.re
	private_token = vTbFeqJYCY3sibBP7BZM
	api_version = 4
	
	[elsewhere]
	url = http://else.whe.re:8080
	private_token = CkqsjqcQSFH5FQKDccu4
	timeout = 1
	
and store It in a private location.

To sign In with this file, we can use login methods in GitLabLogin class

The syntax is:

	log_in = login.GitLabLogin.login_from_file(configname, file_path)

where `configname` is the name of your configuration in the config file, and `file_path` the path of the config file

By example:

	log_in =  login.GitLabLogin.login_from_file('somewhere', '/home/user/.python-gitlab.cfg')

## Structure

This tool is composed by many classes and structures, following this architecture:

![*class diagram*](GM_class_diagram3.png)

The tool use a hierarchical structure, similar to GitLab's web structure. In each class, the tool offers different views in function of user's level access. To get this, each class hierarchy offers a "interface" class, which analyse the access level of current user, and returns the most appropiated object to this.

The class list is this:

- **GitLab Main module (*login.py*)**: This module allows the user to login in the API, and offers utilities to manage Its user or explore GitLab. If the user is an admin, the tools offers extra utils, as create new users.   

	+ **GitLabLogin**: login methods and interface for GitLab Main classes
		* `login(url, priv_token)`: allow login passing access token and URL by parameter
		* `login_from_file`: allow to read config data (token and URL) from a file
		
		These methods can return two different views, depending of user's access level  
		
		- **GitLab**: give access to user's operation calls, and query calls
			+ `get_user()`: returns an User object, to allow user's operations of own user. 
			+ `get_explorer()`: offers an explorer, to allow explore projects, users, or groups in GitLab (only public or with membership)
			+ `get_tools()`: offers some tools to automatize some operation
			
		- **GitLabAdmin**: give access to admin's operation ops
			+ `create_user(user_data: UserData)`: allow to create an user. It returns an User object. Receive as input an UserData object, with this structure:
			  
			  		UserData(username, name, email, password)
			  
				To create an user, It's necessary to fill all the fields. **If the password is not filled, the function will creates an user with random password**. 
				
			+ `preenable_user(user_data: UserData)`: similar to previous function, this create an user with random password and activate It  
		
	- **Users module (*users.py*)**: This module offer tools to get information about an user and, depending of access level, create projects and groups, or access them. In admin user's access level, also allows to remove or change password to an user.
	
		- **User**: "interface" for user's classes. Depending of user's access level, this allow access to different level views
			+ `get()`getter for user's classes. This can returns different views, depending of access level.
				* **UserView**: Minimal view (for visitors). Allow see the profile of a user 
					- `get_id()`: return User ID of the user
					- `get_username()`: return username of the user
					- `get_project(pj_name: str)`: get a project owned by current user (not necessary own user). Returns a Project object. Receives as input the project name 
					  
					  **Advice: To find a project by name, It's necessary to use the project's path, not real project's name**
					  
					  By example: gitlab.com/AlmuHS/gitlab_manager  
					    
					      pj_name=gitlab_manager
					
				* **UserManager**: own user view. Allow create or get projects and groups
					- `create_project(pj_data: ProjectData)`: create a project.   Returns a Project object. 
					
						It receives as input a ProjectData object, with this structure:
						    
					      ProjectData(name, owner, id, visibility, group)
				      
				      To create a project, It's necessary to fill `name` and `visibility` fields.
				      If `visibility` field is not fill, the function will set `public` visibility by default
			      
			      - `create_group(gr_data: GroupData)`: create a new group. Return a Group object. Receives as input a GroupData object, with this structure
			        						
						GroupData(name, path, description, visibility)
						
					To create a group, It's necessary to fill all fields. If `path` is not filled, It takes the name as path. If `description` is not filled, It generate an empty description. If `visibility` is not filled, It takes `'public'` as default visibility.
					
					
				   - `get_group(gr_name: str)`: get a existent group. Return a Group object.  Receives as input the group name  
				   
			   * **UserAdmin**: Admin user view. Allows to an admin doing some operations as current user (not necessary own user).    
			    
				   - `create_project(pj_data: ProjectData)`: create a project to the current user (not necessary own user). Returns a Project object.   
				     
				     Receives as input a ProjectData object. To create a project to current user, It's necessary to fill `name` and `visibility` fields. If visibility is not filled, It takes `'public'` as default visibility.
				    
			      
			      - `update_password(password: str)`: change the password of current user. Receives as input the new password  
			      - `get_email()`: returns the email of current user
			      - `activate()`: activate an user
			      -  `disable()`: disable an user (inverse to activate)
			      -  `block()`: block an user
			      -  `unblock()`: unblock an user (undone a lock)
			      -  `delete()`: delete the user  
			      
  +  **Member module (*member.py*)**: Auxiliar classes to ease the members management in projects and groups.  There are two member classes, depending of user's access level.  
      
      *  **MemberView**: Minimal access level, to visitors. Allow query operations. 
	      -  `get_members()`: show all members of the current group/project. Returns a gitlab's API member object list
	      -  `get_member(name: str)`: get a specific member of the current group/project. Returns a gitlab's API member object. Receive as input the member's username.
	      -  `get_owner()`: get the owner of the current group/object. Returns a gitlab's API member object list  
	      
      *  **MemberAdmin**: Admin access level, for project/group's owners (or admin). 
			
			- `add_member(member_data: MemberData)`: Add a user as a member of a project/group.   
			  
			  Receives as input a MemberData object, with this structure:
			    
			      MemberData(username, access_level, expiration_date)
			      
			  To add a member, It's necessary to fill `username` and `access_level` fields.
			  The access level available are:
			  
			  - `gitlab.DEVELOPER_ACCESS`
			  - `gitlab.GUEST_ACCESS`
			  - `gitlab.MAINTAINER_ACCESS`
			  	   
			  You can read about access level and permissions in [GitLab docs](https://gitlab.com/help/user/permissions)
			  
		    - `change_member_access_level(member_data: MemberData)`: change the access level of a member. Receives as input a MemberData object. To change the access level of a user, It's necessary to fill `username` and `access_level` fields.
		    
		    - `remove_member(username: str)`: remove a member of current project/group. Receive as input the username of the member.
		    

	- **Projects module (*projects.py*)**: This module offer tools to get information about a project and,  depending of the user's access level, manage It.  
	
		- **Project**: "interface" for Project classes. Depending of user's access level, this allow access to different level views
			+ `get()`: getter for Project's classes. This can returns different views, depending of access level.  
			
				* **ProjectView**: Minimal view, for visitors. Allow query ops. This class has access to the methods of MemberView
					- `get_statistics()`: Return a structure with the project' statistics
					- `get_size()`: return the size of the repository
					- `get_id()`: Return the ID of the project
					- `get_snapshot()`: Download a tar.gz file with a snapshot of the project. This snapshot will allow recover the git repository from a previous state
					- `get_archive()`: Download a tar.gz file with the latest content of the master branch of the repository
					
				* **ProjectAdmin**: Full view, for maintainers and owners. Allow management ops. This class has access to the methods of MemberAdmin
					- `archive()`: archive the project as read-only (only owner)
					- `remove_file(filepath: str)`: remove a file in the repository. Receives as input the path of the file
					- `delete()`: delete the project (only owner)  
					
	* **Groups module (*groups.py*)**: This module offers tools to get information about a group and, depending of access level, manage It
		- **Group**: "interface" for Group classes. 
			+ `get()`: getter for Group classes. This can returns different views, depending of user's access level  
			
				* **GroupView**: Minimal view, for visitors. Allow query ops. This class has access to the methods of MemberView
					- `get_id()`: return the ID of the group
					- `get_subgroup(name: str)`: allow to access to a existent subgroup inside the current group. Return a Group object. Receive as input the subgroup name
					- `get_project(project_name: str)`: allow to get a project inside the current group. Return a Project object. Receive as input the project name  
					
				* **GroupDeveloper**: 2nd level view, for group's developers. This class allow to do some basic management ops. 
					- `create_project(pj_data: ProjectData)`: create a project inside the group.   
					  
					  Receives as input a ProjectData object. To create a project inside the group, It's necessary to fill `name` and `visibility` fields. If `visibility` is not filled, the function will take `'public'` as visibility by default  
					  
			  * **GroupMaintainer**: 3rd level view, for group's maintainers. Allows to do advanced management ops.  
			  
				  - `create_subgroup(sg_data: SubGrpupData)`: create a new subgroup inside the current group. Return a Group object.   
				    
				     Receives as input a SubGroupData object, with this structure:
				       
				       	SubGroupData(name, group, path)
			       	
			       	To create a subgroup, It's necessary to fill `name`,  `path` and `visibility`fields.
			       	If `path` field is not filled, the function will take the name value as path. If `visisibility` is not set, the function will take 'public' by default (if the group is not public, the visibility will be set to the same value than group)
			       	
	       	* **GroupAdmin**: Full access view. This class has access to MemberAdmin methods. 
		       	- `delete()`: delete the group  
		       	
   	
    - **Explore module (*public.py*)**: Include the Explore class, which allows to find projects, groups (public or with membership) and users.   
	    + **Explore class**: Main explorer class. 
		    * `find_project(project_data: ProjectData)`: find a public project (or with membership). Returns a Project object.   
		   
		   		Receives as input a ProjectData object. To find a project, It's necessary to fill `name` and `owner` fields. If owner field is not filled, the own user is taken as owner. 
		   		
		   	* `find_project_by_pattern(project_pattern: str)`: get all project named by a specific pattern. Returns a GitLab Project API's list. Receive as input the word pattern.  
		   	
		   	* `find_group_by_pattern(gr_pattern: str)`: find the groups which name match with a pattern. Return a list with the Group objects of the groups which match with the pattern. Receives as input the name pattern. The pattern can be written using regular expressions, using [python.re syntax](https://docs.python.org/3/library/re.html#re-syntax).
		   	
		   	* `find_old_projects(max_days_old: int)`: Get a list of project which creation data is higher than a specific amount of days. Return a list of Gitlab Project API dictionary. Receive as input the max days old allowed for the projects. 
		   	 
		   	* `find_group(gr_name: str)`: find a public group (or with membership). Returns a Group object. Receives as input the group name.
		   	* `find_user(user_name: str)`: find a user by name. Returns a User object. Receives as input the username.  *If the function is executed by an admin, It returns an UserAdmin view, to allow doing management ops to the user.*
		   	 
    - **Tools module (*tools.py*)**	: includes the Tools class, with a collection of tools to automatize complex tasks.   
   	
	   	+	**Tools class**: Main tools class
		   	*	`activate_users_from_file(file_path: str)` (admin only): activate users from a userlist stored in a file.   
		   	
		   		Receive as input the path of a file. The file must have this structure:
		   			
		   			name, surname, [any], email
		   			
		   		By example
		   		
		   			John, Jordan, 0, john_jordan@mymail.com
		   		
		   		The username will be taken from the email name (by example: `user1@mail.com` takes `user1` as username).  
					  
			* `add_user_subgroup_from_file(filepath: str, group_name: str, name_pattern)`: Create a group, creating a subgroup for each user. Receives as input the path of a file (which will have the same structure than previous function), the group name, and a pattern for the subgroup's name.  The group and subgroups visibility will be set as private
			
				The subgroups name will follow the name pattern to set Its name. The name of the subgroup will we created as `name_pattern` followed by the username of Its owner.
				  
				By example, if the pattern is `SG_`, the subgroups could be named as `SG_james`, `SG_richard`, `SG_jack`... etc
			
			* `download_projects_by_subgroups(group_name: str, project_name: str)`: Download the projects with a specific name, stored in the subgroups of a group. Receives as input the group name and the name of the project to download.   
			  
			  The function will recover all subgroups of this group, searching and downloading the projects with this name.  
			    
			  The projects will be stored in a zip file with the name of the group, with a folder for each project, named as the subgroup's owner.  
			    
			  To find the subgroup's owner, the function assume that the owner is the member which name appears in subgroup's name.  
			    
			    By example: `subgroup_james.watson` assume that james.watson is the owner of the subgroup
			    
		    * `download_all_from_group(group_name: str)`: Download all the projects stored in a group, including subgroups in all deep levels. Receives as input the name of the group. Generated a zip file with the same folder structure than the group (group/subgroup/.../project).
			
			* `gen_groupfile_from_pattern(filename: str, group_pattern: str)`: Generate a text file with the name of the groups which match with a pattern. Receives as input the group's name pattern. The pattern can be written using regular expressions, with [python.re syntax](https://docs.python.org/3/library/re.html#re-syntax), and the filename to store the data. **The file only will be generated in case that there are any match with the pattern**  
			
			* `remove_groups_from_file(groupfile: str)`: remove the groups using the names stored in a text file (by example, generated by previous function). Receives as input the filename, in which the group are identified with its name, with a line for each group.
			  
			  By example:  
			    
			   	  group1
			   	  group2
			   	  group3
			   	  
			   **This function only removes groups, NOT SUBGROUPS**
			
			* `remove_projects_by_pattern(project_pattern: str)`: Removes the projects which name follows a specific pattern. 
			
			* `create_projects_from_file(file_path: str)`(admin only): create a collection of projects reading Its name from a file.   
			  
			  The file must store the owner and the name of the repository, with this structure  
			    
			      owner;name
			      
			  by example:
			  
			  	  john_watson; project1
				

## Using examples

### Entering to GitLab Manager

- **LogIn (using private token)**

	  main = login.GitLabLogin.login('https://gitlab.com', 'aBC58Dt')

- **Get user panel**

	  user = main.get_user()

### Own user actions


- **Create a new project**

	  project_data = ProjectData(name="myproject", visibility="public")
	  project = user.create_project(project_data)
	  
- **Get a existent project (owned)**

	  project = user.get_project("myproject")

- **Add members to a project**

	  member_data = MemberData(username="user1", access_level=gitlab.DEVELOPER_ACCESS)
	  project.add_member(member_data)
	  
- **Create a group**

	  group_data = GroupData(name="group1", path="group1", visibility="public")
	  group = user.create_group(group_data)
	  
- **Get a existent group**

	  group = user.get_group("group1")

- **Add members to a group**

	  member_data = MemberData(username="user1", access_level=gitlab.DEVELOPER_ACCESS)
	  group.add_member(member_data)
	  
- **Remove members**

  In a group:
  		
      group.remove_member("user1")
      
  In a project:
     
      project.remove_member("user1")
      
- **Create subgroup**

      sg_data = SubGroupData(name="sg1", path="sg1")
      subgroup = group.create_subgroup(sg_data)
      
- **Get a existent subgroup**

	  subgroup = group.get_subgroup("sg1")
	  
- **Create a project in a group**

      project_data = ProjectData(name="group_project", visibility="public")
	  group_project = group.create_project(project_data)
	  
- **Get a project in a group**

	  group_project = group.get_project("group_project")
	  
- **Remove a project**

	  project.delete()
	  
- **Remove a group**

      group.delete()
      
### Exploration

- **Get explorer**

	  explorer = main.get_explorer()

- **Find another user**

	  user = explorer.find_user("user1")
	  
- **Search a project of another user**

	  project = user.get_project("project2")
	 
- **Find a project using the explorer (not only owned)**

	  project_data = ProjectData(name="project2", owner="user1")
	  project = explorer.find_project(project_data)
	  
- **Find a group (not subgroups, not only owned)**

	  group = explorer.find_group("group1")
	
## Error management

The error management in GitLab Manager is pretty simple: 

- If the function must return an object (Project, User, Group..., etc), the function will return `None` in case of error.

- In some cases, some GitLab API operations can generate an exception (create project/group, add/remove member,... etc). In this case, the function will capture the exception and show the message by screen.  

- In the functions in which no object is expected (add/remove member, delete project... etc), if some function needs a error check, the function can return `True` if success, or `False` if error.

