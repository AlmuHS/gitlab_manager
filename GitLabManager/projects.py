import humanfriendly

from GitLabManager.member import MemberView, MemberAdmin
from GitLabManager.common import *


class Project:
    gl: gitlab
    gl_project: gitlab
    
    def __init__(self, gl: gitlab, gl_project: gitlab):
        self.gl = gl
        self.gl_project = gl_project
        
    def __has_grant(self):
        grant = False
        
        #check if the user is an admin or project's owner
        project_access = self.gl_project.permissions['project_access']
        is_admin = self.gl.user.attributes.get('is_admin', False)
        
        if project_access:
                    access_level = project_access['access_level']
        
        #if the user is admin or project's owner, give Admin access (allow management ops)
        grant = ( ((project_access) and (access_level >= 40)) or is_admin)
        
        return grant
        
    def get(self):
        pj = None
        grant = self.__has_grant()
        
        if grant:
            pj = ProjectAdmin(self.gl, self.gl_project)
        else:
            #if the user is not admin or project's owner, get Regular access (only queries)
            pj = ProjectView(self.gl, self.gl_project)
                        
        return pj
        
    
class ProjectView(MemberView):
    gl: gitlab
    gl_project: gitlab
    
    def __init__(self, gl: gitlab, gl_project: gitlab):
        self.gl = gl
        self.gl_project = gl_project
            
        MemberView.__init__(self, self.gl_project)
    
    def get_pj_namespace(self):        
        return self.gl_project.namespace['full_path']
    
    def get_name(self):
        return self.gl_project.path
    
    def get_statistics(self):
        project_id = self.gl_project.id
        
        #refind project in API to get statistics data
        project = self.gl.projects.get(project_id, statistics=True)

        pj_statistics = project.attributes['statistics']

        return pj_statistics

    def get_size(self):
        statistics = self.get_project_statistics()
        size_bytes = None

        size_bytes = statistics['storage_size']
        size_human_read = humanfriendly.format_size(size_bytes, binary=True)

        print(size_human_read)

        return size_bytes

    def get_id(self):
        project_id = self.gl_project.id

        return project_id
        
    def get_snapshot(self, filename: str = None):
        success = False
             
        if not filename:
            zipfn = f'{self.gl_project.path}_snapshot.tar.gz'
        else:
            zipfn = f'{filename}.tar.gz'
        
        try:
            with open(zipfn, "wb") as f:
                self.gl_project.snapshot(streamed=True, action=f.write)
                success = True
        except Exception as error:
            print(f"Error downloading snapshot: {error}")
            success = False
            
        return success
            
    def get_archive(self, filename: str = None):
        success = False
        
        if not filename:
            zipfn = f'{self.gl_project.path}_archive.tar.gz'
        else:
            zipfn = f'{filename}.tar.gz'
        
        try:
            archive = self.gl_project.repository_archive()
            
            with open(zipfn, "wb") as f:
                f.write(archive)
                success = True
        except Exception as error:
            print(f"Error downloading archive: {error}")
            success = False
        
        return success
            
class ProjectAdmin(ProjectView, MemberAdmin):
    gl: gitlab
    gl_project: gitlab
    
    def __init__(self, gl: gitlab, gl_project: gitlab):
        self.gl = gl
        self.gl_project = gl_project
        
        ProjectView.__init__(self, gl, gl_project)
        MemberAdmin.__init__(self, self.gl_project)
        
        print("Project Admin")
        
    def archive(self):
        self.gl_project.archive()

    def remove_file(self,filepath: str):
        file = self.gl_project.files.get(file_path=filepath, ref='master')
        file.delete(commit_message='Delete file', branch='master')

    def delete(self):
        #delete project
        self.gl_project.delete()
    
    
