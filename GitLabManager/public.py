import re

from GitLabManager.projects import Project
from GitLabManager.groups import Group
from GitLabManager.users import User

from GitLabManager.common import *

class Explore:
    gl: gitlab
    
    def __init__(self, gl: gitlab):
        self.gl = gl
        
    def find_project(self, project_data: ProjectData):
        project = None
        
        #if the owner is not set, take the own user as the owner
        if project_data.owner == None:
            project_data.owner = str(self.gl.user.username)
            
        #find the project owner
        owner = self.find_user(project_data.owner)
        
        if owner:
            #search the project in the project list of its owner
            project = owner.get_project(project_data.name)       
                    
        return project
        
    def find_old_projects(self, max_days_old: int):
        project_list = self.gl.projects.list(all=True)
        
        old_projects = []
        
        for gl_project in project_list:
            creation_date_str = gl_project.attributes['created_at'].split('T')[0]
            creation_date_obj = datetime.strptime(creation_date_str, '%Y-%m-%d').date()

            max_old_date = (datetime.today().date() - timedelta(days=max_days_old))

            if creation_date_obj < max_old_date:
                pj_obj = Project(self.gl, gl_project)
                old_projects.append(pj_obj)        
            
        print(old_projects)

        return old_projects
    
    def find_project_by_pattern(project_pattern: str):
        project_list = self.gl.projects.list(search=project_pattern)
        
        return project_list
        
    def find_group(self, gr_name: str):
        group = None
        
        try:
            gl_group = self.gl.groups.get(gr_name)
            group = Group(self.gl, gl_group).get()
        except Exception as error:
            print(f"Error searching group: {error}")
        
        return group
        
    def find_group_by_pattern(self, gr_pattern: str):
        pattern = re.compile(gr_pattern)
        
        gr_obj_list = []
        group_list = self.gl.groups.list(all=True)

        for group in group_list:
            
            #search groups (excluding subgroups), which match with the regex pattern
            if (pattern.match(group.path)) and (group.path == group.full_path):
                gl_group = self.gl.groups.get(group.id)
                gr_obj = Group(self.gl, gl_group).get()
                print(gr_obj.get_path())
                
                gr_obj_list.append(gr_obj)
        
        if len(gr_obj_list) == 0:
            print("Error: pattern not found")
        
        return gr_obj_list
    
    def find_user(self, user_name: str):
        user_obj = None
        user = self.gl.users.list(username=user_name)
        
        if user:
            user_id = user[0].id
            gl_user = self.gl.users.get(user_id)
            user_obj = User(self.gl, gl_user).get()
        else:
            print("Error: user not found")

        return user_obj

        
