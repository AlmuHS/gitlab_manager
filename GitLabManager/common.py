import gitlab
from dataclasses import dataclass
from enum import Enum

class ProjectError(Exception):
    pass

class GroupError(Exception):
	pass
	
class SGError(Exception):
	pass
    
class MemberError(Exception):
	pass

@dataclass
class ProjectData:
    name: str
    owner: str = None
    id: int = None
    visibility: str = 'public'
    group: str = None   
    
@dataclass
class GroupData:
    name: str
    path: str = None
    description: str = ""
    visibility: str = 'public'

@dataclass
class SubGroupData:
    name: str
    group: str = ""
    path: str = None
    visibility: str = 'public'
    
@dataclass
class UserData:
    username: str
    name: str
    email: str = ""
    password: str = None
    
class AccessLevel(Enum):
    DEVELOPER = gitlab.DEVELOPER_ACCESS
    GUEST = gitlab.GUEST_ACCESS
    MAINTAINER = gitlab.MAINTAINER_ACCESS
    
@dataclass
class MemberData:
    username: str
    access_level: AccessLevel
    expiration_date: str = None
