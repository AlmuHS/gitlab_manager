import random
import string
import re

from GitLabManager.users import User
from GitLabManager.public import Explore
from GitLabManager.tools import Tools

from GitLabManager.common import *

class GitLabLogin:
    gl: gitlab = None
    
    def login(self, url: str, priv_token: str):
        main = None
        
        try:
            self.gl = gitlab.Gitlab(url, private_token=priv_token)
            self.gl.auth()
            main = self.__get()
        except Exception as error:
            print(f"Error in login: {error}")

        return main

    def login_from_file(self, configname: str, file_path: str):
        main = None
        
        try:
            self.gl = gitlab.Gitlab.from_config(configname, [file_path])
            self.gl.auth()
            main = self.__get()
        except Exception as error:
            print(f"Error in login: {error}")
                
        return main

    def gen_config_file(self, url: str, private_token: str, filename: str):
        try:
            #read the contents of original file
            conf_file_src = open("python-gitlab.cfg", "r")
            data = conf_file_src.read()
            conf_file_src.close()
           
            #replace the url and token with the new data            
            data = re.sub(r"url = .*", f"url = {url}", data)
            data = re.sub(r"private_token = .*", f"private_token = {private_token}", data)
            
            #load the new text in the same file    
            with open(filename, "w") as conf_file:
                conf_file.write(data)
        
        except Exception as error:
            print(f"Error generating config file: {error}")

    def __get(self):
        is_admin = self.gl.user.attributes.get('is_admin', False)
         
        if is_admin:
            gitlab_obj = GitLabAdmin(self.gl)
        else:
            gitlab_obj = GitLab(self.gl)
            
        return gitlab_obj
    

class GitLab:
    gl: gitlab

    def __init__(self, gl: gitlab):
        self.gl = gl

    def get_user(self):
        user = None
        user_id = None

        if self.gl:
            user = User(self.gl, self.gl.user).get()
            
        return user

    def get_explorer(self):
        explorer = Explore(self.gl)
        
        return explorer
        
    def get_tools(self):
        return Tools(self)
        
        
class GitLabAdmin(GitLab):
    gl: gitlab
    
    def __init__(self, gl: gitlab):
        self.gl = gl
    
    def __generate_random_password(self, lenght):
        password = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(lenght))
        return password
    
    def create_user(self, userdata: UserData):
        user = None
        gl_user = None
        
        if not userdata.password:
            userdata.password = self.__generate_random_password(128)
        
        try:
            gl_user = self.gl.users.create({'email': userdata.email,
                                            'password': userdata.password,
                                            'username': userdata.username,
                                            'name': userdata.name,
                                            'skip_confirmation': True})
        except Exception as error:
            print(f"Error creating user: {error}")

        if gl_user:
            self.user_data = userdata
            user = User(self.gl, gl_user).get()

        return user
    
    def preenable_user(self, userdata: UserData):
        user = None
        userdata.password = self.__generate_random_password(128)
        try:
            user = self.create_user(userdata)
            user.activate()
        except:
            print(f"Error creating user {userdata.username}") 
            
        return user
