from datetime import datetime, timedelta
import time
import zipfile
import tarfile
import os
import shutil

import GitLabManager.login
import GitLabManager.projects
import GitLabManager.users
import GitLabManager.groups

from GitLabManager.common import *

class Tools:
    gl: gitlab
    gl_manager: GitLabManager
    
    def __init__(self, gl_manager: GitLabManager):
        self.gl_manager = gl_manager
        
    def __get_user_list(self, filename):
        user_list = []
        
        try:
            #open file as read-only
            with open(filename, 'r') as file:
                
                #skip header
                next(file)
                
                for user in file:
                    #get user data
                    userdata = self.__get_user_data(user)
            
                    #add user to userlist
                    user_list.append(userdata)
        except Exception as error:
            print(f"Error reading users file: {error}")        
        
        return user_list        

    def __get_user_data(self, user_string: str):
        userdata = user_string.split(',')
        
        name = f'{userdata[0]} {userdata[1]}'
        name = name.replace('\"', "")
        print(f'name: {name}', end = '\t')
        
        email = userdata[3]
        print(f'email: {email}', end = '\t')
        
        user_name = email.split('@')[0]
        print(f'user: {user_name}')
        
        data = {'email':email, 'username': user_name, 'name': name}
        
        return data


    def __create_subgroup_with_member(self, group, subgroup_name, member_name):        
        subg_data = SubGroupData(name=subgroup_name, visibility='private')
        subg_obj = group.create_subgroup(subg_data)
        
        message = {True: "OK", False:"ERROR"}
        
        #if the subgroup was created successfully, add user as a new member
        member_added = False
        
        if subg_obj:
            subg_member = MemberData(username=member_name, access_level=gitlab.OWNER_ACCESS)
            member_added = subg_obj.add_member(subg_member)
            
            print(f"{member_name}\t{subgroup_name}", end = '\t')
        
        success = (subg_obj and member_added)
        
        #show confirmation message
        print(message[success])
        
        return success
        
    def activate_users_from_file(self, file_path):
        user_list = self.__get_user_list(file_path)
            
        #read file line to line
        for user in user_list:
            user_data = UserData(username=user['username'], name=user['name'], email=user['email'])
            self.gl_manager.preenable_user(user_data)
    
    def add_users_subgroup_from_file(self, filepath, group_name, name_pattern):
        #status variable, to advice about success or fail at finish
        success = False
        group = None
        
        #get user panel to create groups
        user = self.gl_manager.get_user()
        
        #get users list
        userlist = self.__get_user_list(filepath)
        
        if userlist:
            #create new group
            gr_data = GroupData(name=group_name, visibility='private')
            group = user.create_group(gr_data)
        
            #if the group was created successfully
            if group:
                #member counter, to detect if there was any error adding users
                users = 0; members = 0
                
                for user in userlist:
                    #increment user counter
                    users += 1
                    
                    #create new subgroup
                    subgroup_name = f"{name_pattern}{user['username']}"
                    sg_added = self.__create_subgroup_with_member(group, subgroup_name, user['username'])
                    
                    if sg_added:
                        members += 1
            
                print(f"{members} of {users} users added to subgroups")
                
                if users == members:
                    success = True
        
        return group, success
        
    def __copy_tar_to_zip(self, tar_file, zip_file, maindir_name = None):
        success = False
        
        try:
            with zipfile.ZipFile(zip_file, 'a') as end_zip:
                with tarfile.open(tar_file) as src_tar:
                    filelist = src_tar.getmembers()
                    
                    #get the source directory name
                    dir_name = str(filelist[0].name)
                    
                    #copy each file from tar to zip
                    for file in filelist:
                        #replace original directory name, with the maindir name
                        filename = str(file.name).replace(dir_name, maindir_name)
                        
                        #extract the file and copy it in the destination zip                           
                        src_tar.extract(file)
                        end_zip.write(file.name, filename)
                
                    #remove extracted directory
                    shutil.rmtree(dir_name)
                    
            #remove original tar file
            os.remove(tar_file)
            success = True
            
        except Exception as error:
            print(f"Error copying files to zip file: {error}")
            success = False
        
        return success
    
    def __get_sg_project_owner(self, group, subgroup):
        owner = None
        
        gr_members_list = group.get_members()
        sg_members_list = subgroup.get_members()
        
        for sg_member in sg_members_list:
            #the owner is the member which name appears in subgroup name
            if sg_member.username in subgroup.get_name():
                owner = sg_member.username
                break
    
        return owner
    
    
    def __get_all_projects_from_group(self, group_name: str):
        projects_list = []
        pj_list = []
        explorer = self.gl_manager.get_explorer()
        group = explorer.find_group(group_name)
                
        if group:
            pj_list = self.__get_all_projects_from_subgroup(group, projects_list)
        
        return pj_list
    
    
    def __get_all_projects_from_subgroup(self, group, projects_list: list):
        projects_list += group.get_projects_list()
        sg_list = group.get_subgroups_list()
        
        if sg_list:
            for sg in sg_list:
                self.__get_all_projects_from_subgroup(sg, projects_list)
                
        return projects_list
    
    def download_all_from_group(self, group_name: str):
        success = False
        
        projects_list = self.__get_all_projects_from_group(group_name)
                
        #remove previous zip file, if exists
        zip_name = f"{group_name}.zip"
        try:
            os.remove(zip_name)
        except:
            None
        
        project_number = len(projects_list)
        pj_downloaded = 0
        
        for project in projects_list:
            namespace = project.get_pj_namespace()
            pj_name = project.get_name()
            
            maindir = f"{namespace}/{pj_name}"
            print(maindir, end='\t')
            
            archive = project.get_archive(pj_name)
            
            if archive:
                tar_name = f"{pj_name}.tar.gz"
                download = self.__copy_tar_to_zip(tar_name, zip_name, maindir)
                
                if download:
                    print("OK")
                    pj_downloaded += 1
        
        print(f"Downloaded {pj_downloaded} of {project_number} projects")
        
        if (pj_downloaded == project_number) and (pj_downloaded > 0):
            success = True
            
        return success
            
    
    def download_projects_by_subgroups(self, group_name, project_name):
        success = False
        
        #load explorer
        explorer = self.gl_manager.get_explorer()
        
        #find the group
        group = explorer.find_group(group_name)
        
        if group:
            sg_list = group.get_subgroups_list()
            
            sg_number = len(sg_list)
            project_number = 0
            
            for subgroup in sg_list:
                subgroup_name = subgroup.get_name()
                print(subgroup_name)
                
                project = subgroup.get_project(project_name)
                
                if project:
                    owner = self.__get_sg_project_owner(group, subgroup)
                    
                    #the file will be stored using owner name
                    downloaded = project.get_archive(owner)
                    
                    if downloaded:
                        zip_name = f'{group_name}.zip'
                        tar_name = f"{owner}.tar.gz"
                        copied = self.__copy_tar_to_zip(tar_name, zip_name, owner)
                        project_number += 1
                        
            
            print(f"{project_number} projects downloaded from {sg_number} subgroups")
                    
            if (sg_number == project_number) and (sg_number > 0):
                success = True
                
            return success
    
    def remove_projects_by_pattern(self, project_pattern: str):
        pj_list = public.find_project_by_pattern(project_pattern)

        for pj in pj_list:
            project = gl.projects.get(pj.id)
            project.delete()


    def create_projects_from_file(self, file_path):
        explorer = self.gl_manager.get_explorer()
        success = False
        
        try:
            with open(file_path, 'r') as file:
                lines = 0
                projects = 0
                
                for project in file:
                    lines += 1
                    
                    data = project.split(";")
                    pj_owner = data[0]
                    pj_name = data[1]

                    pj_data = ProjectData(name=pj_name, owner=pj_owner)

                    owner = explorer.find_user(pj_owner)
                    
                    if owner:
                        project = owner.create_project(pj_data)
                        
                        if project:
                            projects += 1
                        
                    
                if lines == projects:
                    success = True
        except Exception as error:
            print(f"Error creating projects: {error}")
            success = False
                    
        return success
        
    def gen_groupfile_from_pattern(self, filename: str, group_pattern: str):
        success = False
        explorer = self.gl_manager.get_explorer()

        group_list = explorer.find_group_by_pattern(group_pattern)
        if len(group_list) > 0:
            try:
                with open(filename, 'w') as group_file:
                    for group in group_list:
                        group_path = group.get_path()
                        group_file.write(f"{group_path}\n")
                    success = True
            except Exception as error:
                print(f"Error writing groupfile: {error}")
                success = False
        else:
            success = False

        return success

    def remove_groups_from_file(self, groupfile: str):
        success = False
        explorer = self.gl_manager.get_explorer()
        
        try:
            with open(groupfile, 'r') as group_file:
                
                for group_name in group_file:
                    group = explorer.find_group(group_name.rstrip())
                    
                    if group:
                        group.delete()
                        success = True
        except Exception as error:
            print(f"Error removing groups: {error}")
            success = False
            
        return success
        
    def change_member_access_level_in_subgroups(self, group_name, member_file):
        explorer = self.gl_manager.get_explorer()
        group = explorer.find_group(group_name)
        
        changed = 0
        
        if group:
            sg_list = group.get_subgroups_list()
            user_list = self.__get_user_list(member_file)
            
            users = len(user_list)
            
            for user in user_list:
                sg_name = f"{group_name}_{user['username']}"
                print(sg_name)
                
                subgroup = group.get_subgroup(sg_name)
                member_data = MemberData(username=f"{user['username']}", access_level=gitlab.OWNER_ACCESS)
                
                change = subgroup.change_member_access_level(member_data)
            
                if change:
                    print(f"{user['username']} access level changed to owner")
                    changed += 1
                else:
                    print(f"Error changing access level to {user['username']}")
                    
            print(f"changed {changed} of {users} users")
        
