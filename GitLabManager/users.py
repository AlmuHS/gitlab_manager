from GitLabManager.projects import Project
from GitLabManager.groups import Group
from GitLabManager.member import MemberView, MemberAdmin
from GitLabManager.common import *

class User:
    gl: gitlab
    gl_user: gitlab
    
    def __init__(self, gl: gitlab, gl_user: gitlab):
        self.gl = gl
        self.gl_user = gl_user
    
    def __get_level_access(self):
        access_level = 0
        is_admin = self.gl.user.attributes.get('is_admin', False)
        
        if (self.gl.user.username == self.gl_user.username) and (not is_admin):
            access_level = 'own'
        elif is_admin:
            access_level = 'admin'
        else:
            access_level = 'visitor'
            
        return access_level
            
    def get(self):
        user_obj = None
        access_level = self.__get_level_access()
    
    
        user_select = {'own': UserManager(self.gl, self.gl_user),
                       'admin': UserAdmin(self.gl, self.gl_user),
                       'visitor': UserView(self.gl, self.gl_user)}
                       
        user_obj = user_select[access_level]
                       
        return user_obj


class UserView:
    gl: gitlab
    gl_user: gitlab = None

    def __init__(self, gl: gitlab, gl_user: gitlab):
        self.gl = gl
        self.gl_user = gl_user

    def get_id(self):
        return self.gl_user.id

    def get_username(self):
        return self.gl_user.username
    
    def is_admin(self):
        is_admin = self.gl.user.attributes.get('is_admin', False)
        
        return is_admin
    
    def get_project(self, pj_name: str):
        project = None
        
        #refind user to get project list caller
        user = self.gl.users.list(username=self.gl_user.username)[0]
        
        #search the project in the projects list of this user
        project_list = user.projects.list(search=pj_name)
        project_id = None
        
        for pj in project_list:
            if pj.path == pj_name:
                project_id = pj.id
                break
    
        if project_id:
            gl_project = self.gl.projects.get(project_id)
            project = Project(self.gl, gl_project).get()
        else:
            print("Error: project not found (or Its user is not the owner)")
                
        return project

class UserManager(UserView):
    gl: gitlab
    gl_user: gitlab = None
    user_data: UserData = None

    def create_project(self, pj_data: ProjectData):
        pj = None
        project = None
        
        try:
            project = self.gl.projects.create({'name': pj_data.name, 
                                               'visibility': pj_data.visibility})
                                               
            gl_project = self.gl.projects.get(project.id)
            pj = Project(self.gl, gl_project).get()
        except Exception as error:
            print(f"Error in project {pj_data.name} creation: {error}")    

        return pj


    def create_group(self, gr_data: GroupData):
        gr = None

        if not gr_data.path:
            gr_data.path = gr_data.name
                    
        try:                
            group = self.gl.groups.create({'name': gr_data.name, 
                                           'path': gr_data.path,
                                           'visibility': gr_data.visibility})
                                           
            gl_group = self.gl.groups.get(gr_data.id)
            gr = Group(self.gl, gl_group).get()
        except Exception as error:
            print(f"Error in group {gr_data.name} creation: {error}")
        
        return gr

    def get_group(self, gr_name: str):
        gr = None
        group = None
        
        try:         
            gl_group = self.gl.groups.get(gr_name)
            gr = Group(self.gl, gl_group).get()
        except Exception as error:
            print(f"Error: group not found: {error}")
 
        return gr
        
    def update_password(self, password: str):
        self.gl_user.password = password
        self.gl_user.save() 


class UserAdmin(UserManager):
    gl: gitlab
    gl_user: gitlab
    user_data: UserData

    def __init__(self, gl: gitlab, gl_user: gitlab):
        self.gl = gl
        self.gl_user = gl_user
        _name = self.gl_user.name
        _username = self.gl_user.username
        self.user_data = UserData(username=_username, name=_name) 
        
        
    def create_project(self, pj_data: ProjectData):
        pj = None

        try:
            project = self.gl_user.projects.create({'name': pj_data.name, 
                                                    'visibility': pj_data.visibility})
                                                    
            gl_project = self.gl.projects.get(project.id)
            pj = Project(self.gl, gl_project).get()
        except Exception as error:
            print(f"Error creating project: {error}")

        return pj
        
    def create_group(self, gr_data: GroupData):
        gr = None
        group = None

        if not gr_data.path:
            gr_data.path = gr_data.name

        try:                
            group = self.gl.groups.create({'name': gr_data.name, 
                                           'path': gr_data.path,
                                           'visibility': gr_data.visibility})
            
            gl_group = self.gl.groups.get(group.id)
            gr = Group(self.gl, gl_group).get()
            
            #if the group is created by an admin (and not by own user), add user to group and exit admin
            if self.gl.user.username != self.gl_user.username:
                real_owner = self.gl_user.username
                admin = self.gl.user.username
                owner_data = member.MemberData(username=real_owner, access_level=gitlab.OWNER_ACCESS)
                gr.add_member(owner_data)
                gr.remove_member(admin)
                
        except Exception as error:
            print(f"Error in group {gr_data.name} creation: {error}")
    
        return gr
    
    def update_password(self, password: str):
        self.gl_user.password = password
        self.gl_user.save() 
        
    def get_email(self):
        return self.user_data.email
        
    def activate(self):
        self.gl_user.activate()

    def disable(self):
        self.gl_user.deactivate()
    
    def block(self):
        self.gl_user.block()

    def unblock(self):
        self.gl_user.unblock()
            
    def delete(self):        
        self.gl_user.delete()

