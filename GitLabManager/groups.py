from GitLabManager.projects import Project
from GitLabManager.member import MemberView, MemberAdmin

from GitLabManager.common import *

class Group:
    gl: gitlab
    gl_group: gitlab
    
    def __init__(self, gl: gitlab, gl_group):
        self.gl = gl
        self.gl_group = gl_group
        
        MemberView.__init__(self, self.gl_group)

    def __get_level_access(self):
        ACCESS_LEVEL = {0: 'Guest', 10:'Guest', 20:'Reporter', 30:'Developer', 40: 'Maintainer', 50: 'Owner'}
              
        #check if the user access level
        is_admin = self.gl.user.attributes.get('is_admin', False)
        
        try:
            access_level = self.gl_group.members.get(self.gl.user.id).access_level
        except:
            access_level = 0
        
        #if the user is admin, give Maintainer access level
        if is_admin:
            access_level = 50
                
        return ACCESS_LEVEL[access_level]

        
    def get(self):
        group = None
        group_select = {'Owner': GroupAdmin(self.gl, self.gl_group),
                        'Maintainer': GroupMaintainer(self.gl, self.gl_group),
                        'Developer': GroupDeveloper(self.gl, self.gl_group),
                        'Guest': GroupView(self.gl, self.gl_group)}
                    
        access_level = self.__get_level_access()
    
        group = group_select[access_level]
        
        return group
      

class GroupView(MemberView):
    name: str
    gl: gitlab
    gl_group: gitlab

    def __init__(self, gl: gitlab, gl_group: gitlab):
        self.gl = gl
        self.gl_group = gl_group

        MemberView.__init__(self, self.gl_group)
            
    def get_id(self):        
        return self.gl_group.id
        
    def get_name(self):
        return self.gl_group.name

    def get_path(self):
        return self.gl_group.full_path

    def get_subgroups_list(self):
        subgroup_list = self.gl_group.subgroups.list(all=True)
        sg_obj_list = []
        
        for subgroup in subgroup_list:
            gl_group = self.gl.groups.get(subgroup.id)
            sg_obj = Group(self.gl, gl_group).get()
            sg_obj_list.append(sg_obj)
        
        return sg_obj_list

    def get_subgroup(self, sg_name: str):
        subg = None        
        subgroup_id = None

        subgroup_list = self.gl_group.subgroups.list(search=sg_name)
        
        if subgroup_list:
            for sg in subgroup_list:
                if sg.path == name:
                  subgroup_id = sg.id
        
        #if the subgroup is found, generate a Group object and return It
        if subgroup_id:
            gl_group = self.gl.groups.get(subgroup_id)
            subg = Group(self.gl, gl_group).get()
               
        return subg


    def get_projects_list(self):
        pj_obj_list = []
        
        project_list = self.gl_group.projects.list(all=True)
        
        for pj in project_list:
            gl_project = self.gl.projects.get(pj.id)
            pj_obj = Project(self.gl, gl_project).get()
            pj_obj_list.append(pj_obj)
            
        return pj_obj_list

    def get_project(self, pj_name: str):
        pj_obj = None
        project_id = None
        
        #search project by name pattern
        project_list = self.gl_group.projects.list(search=pj_name)
        project = None

        #if there are one or more project with this pattern, search the exact ocurrence
        if project_list:
            for pj in project_list:
                if pj.path == pj_name:
                    project_id = pj.id
                    break
        
        if project_id:
            gl_project = self.gl.projects.get(project_id)
            pj_obj = Project(self.gl, gl_project).get()
        else:
            print("Error: the project doesn't exists!!")
        
        return pj_obj

class GroupDeveloper(GroupView):
    gl: gitlab
    gl_group: gitlab
    
    def create_project(self, pj_data: ProjectData):
        pj_obj = None 
        group_id = self.gl_group.id
        
        if self.gl_group.visibility != 'public' and self.pj_data.visibility == 'public':
            pj_data.visibility = self.gl_group.visibility
        
        try:
            project = self.gl.projects.create({'name': pj_data.name,
                                               'visibility': pj_data.visibility,
                                               'namespace_id': group_id})
                                               
            gl_project = self.gl.projects.get(project.id)
            pj_obj = Project(self.gl, gl_project).get()
        except Exception as error:
            print(f"Error in project creation: {error}")

        return pj_obj

class GroupMaintainer(GroupDeveloper):
    
    def create_subgroup(self, sg_data: SubGroupData):
        subg = None
    
        if not sg_data.path:
            sg_data.path = sg_data.name

        if self.gl_group.visibility != 'public' and self.sg_data.visibility == 'public':
            sg_data.visibility = self.gl_group.visibility

        group_id = self.gl_group.id
        
        try:
            gl_subgroup = self.gl.groups.create({'name': sg_data.name, 
                                                 'parent_id': group_id,
                                                 'visibility': sg_data.visibility,
                                                 'path': sg_data.path})
                                                 
            
            gl_group = self.gl.groups.get(gl_subgroup.id)                                     
            subg = Group(self.gl, gl_group).get()
        except Exception as error:
            print(f"Error creating subgroup. {error}")

        return subg

class GroupAdmin(GroupMaintainer, MemberAdmin):
    gl: gitlab
    gl_group: gitlab
    
    def __init__(self, gl: gitlab, gl_group: gitlab):
        self.gl = gl
        self.gl_group = gl_group
        MemberAdmin.__init__(self, gl_group)
        
    def delete(self):
        self.gl_group.delete()
