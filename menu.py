# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'menu3.ui',
# licensing of 'menu3.ui' applies.
#
# Created: Tue Mar 10 23:43:29 2020
#      by: pyside2-uic  running on PySide2 5.14.1
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets
from GitLabManager import GitLabLogin, ProjectData, UserData

class Ui_mainWindow(object):
    def __init__(self):
        self.GitLab = None
    
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(627, 624)
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setMinimumSize(QtCore.QSize(627, 624))
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.online_label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setItalic(True)
        font.setBold(True)
        self.online_label.setFont(font)
        self.online_label.setCursor(QtCore.Qt.ArrowCursor)
        self.online_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.online_label.setObjectName("online_label")
        self.verticalLayout_3.addWidget(self.online_label)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_1 = QtWidgets.QWidget()
        self.tab_1.setObjectName("tab_1")
        self.label_15 = QtWidgets.QLabel(self.tab_1)
        self.label_15.setGeometry(QtCore.QRect(180, 30, 201, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_15.setFont(font)
        self.label_15.setObjectName("label_15")
        self.label_14 = QtWidgets.QLabel(self.tab_1)
        self.label_14.setGeometry(QtCore.QRect(180, 230, 181, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_14.setFont(font)
        self.label_14.setObjectName("label_14")
        self.label_18 = QtWidgets.QLabel(self.tab_1)
        self.label_18.setGeometry(QtCore.QRect(40, 80, 121, 20))
        self.label_18.setObjectName("label_18")
        self.loginButton = QtWidgets.QPushButton(self.tab_1)
        self.loginButton.setGeometry(QtCore.QRect(430, 290, 88, 27))
        self.loginButton.setObjectName("loginButton")
        self.loginURLTextEdit = QtWidgets.QPlainTextEdit(self.tab_1)
        self.loginURLTextEdit.setGeometry(QtCore.QRect(170, 260, 221, 31))
        self.loginURLTextEdit.setObjectName("loginURLTextEdit")
        self.loginTokenTextEdit = QtWidgets.QPlainTextEdit(self.tab_1)
        self.loginTokenTextEdit.setGeometry(QtCore.QRect(170, 300, 221, 31))
        self.loginTokenTextEdit.setObjectName("loginTokenTextEdit")
        self.login_fileButton = QtWidgets.QPushButton(self.tab_1)
        self.login_fileButton.setGeometry(QtCore.QRect(420, 110, 111, 27))
        self.login_fileButton.setObjectName("login_fileButton")
        self.label_16 = QtWidgets.QLabel(self.tab_1)
        self.label_16.setGeometry(QtCore.QRect(30, 120, 141, 20))
        self.label_16.setObjectName("label_16")
        self.login_conffileTextEdit = QtWidgets.QPlainTextEdit(self.tab_1)
        self.login_conffileTextEdit.setGeometry(QtCore.QRect(170, 70, 211, 31))
        self.login_conffileTextEdit.setObjectName("login_conffileTextEdit")
        self.status_login_label = QtWidgets.QLabel(self.tab_1)
        self.status_login_label.setGeometry(QtCore.QRect(200, 460, 261, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setItalic(True)
        font.setBold(True)
        self.status_login_label.setFont(font)
        self.status_login_label.setStyleSheet("selection-color: rgb(115, 210, 22);")
        self.status_login_label.setTextFormat(QtCore.Qt.PlainText)
        self.status_login_label.setScaledContents(True)
        self.status_login_label.setObjectName("status_login_label")
        self.confToolButton = QtWidgets.QToolButton(self.tab_1)
        self.confToolButton.setGeometry(QtCore.QRect(390, 70, 27, 26))
        self.confToolButton.setObjectName("confToolButton")
        self.label_17 = QtWidgets.QLabel(self.tab_1)
        self.label_17.setGeometry(QtCore.QRect(110, 310, 51, 19))
        self.label_17.setObjectName("label_17")
        self.label_6 = QtWidgets.QLabel(self.tab_1)
        self.label_6.setGeometry(QtCore.QRect(120, 270, 41, 19))
        self.label_6.setObjectName("label_6")
        self.login_confnameTextEdit = QtWidgets.QPlainTextEdit(self.tab_1)
        self.login_confnameTextEdit.setGeometry(QtCore.QRect(170, 110, 211, 31))
        self.login_confnameTextEdit.setObjectName("login_confnameTextEdit")
        self.gen_conf_fileCheckBox = QtWidgets.QCheckBox(self.tab_1)
        self.gen_conf_fileCheckBox.setGeometry(QtCore.QRect(170, 380, 221, 25))
        self.gen_conf_fileCheckBox.setObjectName("gen_conf_fileCheckBox")
        self.new_conf_fileTextEdit = QtWidgets.QPlainTextEdit(self.tab_1)
        self.new_conf_fileTextEdit.setEnabled(False)
        self.new_conf_fileTextEdit.setGeometry(QtCore.QRect(170, 340, 221, 31))
        self.new_conf_fileTextEdit.setObjectName("new_conf_fileTextEdit")
        self.new_conf_file_label = QtWidgets.QLabel(self.tab_1)
        self.new_conf_file_label.setEnabled(False)
        self.new_conf_file_label.setGeometry(QtCore.QRect(40, 350, 121, 20))
        self.new_conf_file_label.setObjectName("new_conf_file_label")
        self.tabWidget.addTab(self.tab_1, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.groupsgTextEdit = QtWidgets.QPlainTextEdit(self.tab_2)
        self.groupsgTextEdit.setGeometry(QtCore.QRect(180, 80, 211, 31))
        self.groupsgTextEdit.setObjectName("groupsgTextEdit")
        self.downpjButton = QtWidgets.QPushButton(self.tab_2)
        self.downpjButton.setGeometry(QtCore.QRect(410, 290, 141, 27))
        self.downpjButton.setObjectName("downpjButton")
        self.pjdownTextEdit = QtWidgets.QPlainTextEdit(self.tab_2)
        self.pjdownTextEdit.setGeometry(QtCore.QRect(180, 300, 211, 31))
        self.pjdownTextEdit.setObjectName("pjdownTextEdit")
        self.UsersgfiletoolButton = QtWidgets.QToolButton(self.tab_2)
        self.UsersgfiletoolButton.setGeometry(QtCore.QRect(400, 160, 27, 26))
        self.UsersgfiletoolButton.setObjectName("UsersgfiletoolButton")
        self.grouppjTextEdit = QtWidgets.QPlainTextEdit(self.tab_2)
        self.grouppjTextEdit.setGeometry(QtCore.QRect(180, 259, 211, 31))
        self.grouppjTextEdit.setObjectName("grouppjTextEdit")
        self.label_39 = QtWidgets.QLabel(self.tab_2)
        self.label_39.setGeometry(QtCore.QRect(80, 270, 91, 20))
        self.label_39.setObjectName("label_39")
        self.sgTextEdit = QtWidgets.QPlainTextEdit(self.tab_2)
        self.sgTextEdit.setGeometry(QtCore.QRect(180, 120, 211, 31))
        self.sgTextEdit.setObjectName("sgTextEdit")
        self.label_21 = QtWidgets.QLabel(self.tab_2)
        self.label_21.setGeometry(QtCore.QRect(80, 90, 91, 20))
        self.label_21.setObjectName("label_21")
        self.label_22 = QtWidgets.QLabel(self.tab_2)
        self.label_22.setGeometry(QtCore.QRect(180, 40, 231, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_22.setFont(font)
        self.label_22.setObjectName("label_22")
        self.create_sgButton = QtWidgets.QPushButton(self.tab_2)
        self.create_sgButton.setGeometry(QtCore.QRect(430, 110, 121, 27))
        self.create_sgButton.setObjectName("create_sgButton")
        self.label_41 = QtWidgets.QLabel(self.tab_2)
        self.label_41.setGeometry(QtCore.QRect(80, 310, 91, 20))
        self.label_41.setObjectName("label_41")
        self.label_19 = QtWidgets.QLabel(self.tab_2)
        self.label_19.setGeometry(QtCore.QRect(60, 130, 111, 20))
        self.label_19.setObjectName("label_19")
        self.label_40 = QtWidgets.QLabel(self.tab_2)
        self.label_40.setGeometry(QtCore.QRect(170, 220, 241, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_40.setFont(font)
        self.label_40.setObjectName("label_40")
        self.status_tools_label = QtWidgets.QLabel(self.tab_2)
        self.status_tools_label.setGeometry(QtCore.QRect(200, 460, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setItalic(True)
        font.setBold(True)
        self.status_tools_label.setFont(font)
        self.status_tools_label.setStyleSheet("selection-color: rgb(115, 210, 22);")
        self.status_tools_label.setTextFormat(QtCore.Qt.PlainText)
        self.status_tools_label.setScaledContents(True)
        self.status_tools_label.setObjectName("status_tools_label")
        self.label_20 = QtWidgets.QLabel(self.tab_2)
        self.label_20.setGeometry(QtCore.QRect(100, 170, 67, 19))
        self.label_20.setObjectName("label_20")
        self.UsersgfileTextEdit = QtWidgets.QPlainTextEdit(self.tab_2)
        self.UsersgfileTextEdit.setGeometry(QtCore.QRect(180, 160, 211, 31))
        self.UsersgfileTextEdit.setObjectName("UsersgfileTextEdit")
        self.downgroupTextEdit = QtWidgets.QPlainTextEdit(self.tab_2)
        self.downgroupTextEdit.setGeometry(QtCore.QRect(180, 390, 211, 31))
        self.downgroupTextEdit.setObjectName("downgroupTextEdit")
        self.label_7 = QtWidgets.QLabel(self.tab_2)
        self.label_7.setGeometry(QtCore.QRect(80, 400, 91, 20))
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(self.tab_2)
        self.label_8.setGeometry(QtCore.QRect(180, 360, 221, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.downgrouppushButton = QtWidgets.QPushButton(self.tab_2)
        self.downgrouppushButton.setGeometry(QtCore.QRect(410, 390, 141, 27))
        self.downgrouppushButton.setObjectName("downgrouppushButton")
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.group_patternTextEdit = QtWidgets.QPlainTextEdit(self.tab_3)
        self.group_patternTextEdit.setGeometry(QtCore.QRect(160, 90, 221, 31))
        self.group_patternTextEdit.setObjectName("group_patternTextEdit")
        self.label = QtWidgets.QLabel(self.tab_3)
        self.label.setGeometry(QtCore.QRect(50, 100, 101, 20))
        self.label.setObjectName("label")
        self.newgroupfileTextEdit = QtWidgets.QPlainTextEdit(self.tab_3)
        self.newgroupfileTextEdit.setGeometry(QtCore.QRect(160, 130, 221, 31))
        self.newgroupfileTextEdit.setObjectName("newgroupfileTextEdit")
        self.label_2 = QtWidgets.QLabel(self.tab_3)
        self.label_2.setGeometry(QtCore.QRect(80, 140, 61, 19))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.tab_3)
        self.label_3.setGeometry(QtCore.QRect(170, 50, 231, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gen_groupsfileButton = QtWidgets.QPushButton(self.tab_3)
        self.gen_groupsfileButton.setGeometry(QtCore.QRect(410, 110, 131, 27))
        self.gen_groupsfileButton.setObjectName("gen_groupsfileButton")
        self.groupfileTextEdit = QtWidgets.QPlainTextEdit(self.tab_3)
        self.groupfileTextEdit.setGeometry(QtCore.QRect(163, 240, 221, 31))
        self.groupfileTextEdit.setObjectName("groupfileTextEdit")
        self.label_4 = QtWidgets.QLabel(self.tab_3)
        self.label_4.setGeometry(QtCore.QRect(160, 200, 231, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.tab_3)
        self.label_5.setGeometry(QtCore.QRect(70, 250, 81, 20))
        self.label_5.setObjectName("label_5")
        self.remove_groupsButton = QtWidgets.QPushButton(self.tab_3)
        self.remove_groupsButton.setGeometry(QtCore.QRect(420, 240, 111, 27))
        self.remove_groupsButton.setObjectName("remove_groupsButton")
        self.status_groups_label = QtWidgets.QLabel(self.tab_3)
        self.status_groups_label.setGeometry(QtCore.QRect(190, 460, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setItalic(True)
        font.setBold(True)
        self.status_groups_label.setFont(font)
        self.status_groups_label.setStyleSheet("selection-color: rgb(115, 210, 22);")
        self.status_groups_label.setTextFormat(QtCore.Qt.PlainText)
        self.status_groups_label.setScaledContents(True)
        self.status_groups_label.setObjectName("status_groups_label")
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.label_11 = QtWidgets.QLabel(self.tab_4)
        self.label_11.setGeometry(QtCore.QRect(77, 90, 81, 20))
        self.label_11.setObjectName("label_11")
        self.label_13 = QtWidgets.QLabel(self.tab_4)
        self.label_13.setGeometry(QtCore.QRect(187, 60, 171, 20))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_13.setFont(font)
        self.label_13.setObjectName("label_13")
        self.create_pjfileButton = QtWidgets.QPushButton(self.tab_4)
        self.create_pjfileButton.setGeometry(QtCore.QRect(427, 90, 111, 27))
        self.create_pjfileButton.setObjectName("create_pjfileButton")
        self.pjfiletoolButton = QtWidgets.QToolButton(self.tab_4)
        self.pjfiletoolButton.setGeometry(QtCore.QRect(387, 90, 27, 26))
        self.pjfiletoolButton.setObjectName("pjfiletoolButton")
        self.pjfileTextEdit = QtWidgets.QPlainTextEdit(self.tab_4)
        self.pjfileTextEdit.setGeometry(QtCore.QRect(170, 90, 211, 31))
        self.pjfileTextEdit.setObjectName("pjfileTextEdit")
        self.pjnameTextEdit = QtWidgets.QPlainTextEdit(self.tab_4)
        self.pjnameTextEdit.setGeometry(QtCore.QRect(170, 190, 211, 31))
        self.pjnameTextEdit.setObjectName("pjnameTextEdit")
        self.pjusernameTextEdit = QtWidgets.QPlainTextEdit(self.tab_4)
        self.pjusernameTextEdit.setGeometry(QtCore.QRect(170, 230, 211, 31))
        self.pjusernameTextEdit.setObjectName("pjusernameTextEdit")
        self.label_28 = QtWidgets.QLabel(self.tab_4)
        self.label_28.setGeometry(QtCore.QRect(180, 160, 211, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_28.setFont(font)
        self.label_28.setObjectName("label_28")
        self.label_29 = QtWidgets.QLabel(self.tab_4)
        self.label_29.setGeometry(QtCore.QRect(70, 200, 91, 20))
        self.label_29.setObjectName("label_29")
        self.label_30 = QtWidgets.QLabel(self.tab_4)
        self.label_30.setGeometry(QtCore.QRect(80, 240, 67, 19))
        self.label_30.setObjectName("label_30")
        self.label_31 = QtWidgets.QLabel(self.tab_4)
        self.label_31.setGeometry(QtCore.QRect(100, 270, 61, 21))
        self.label_31.setObjectName("label_31")
        self.pj_visi_comboBox = QtWidgets.QComboBox(self.tab_4)
        self.pj_visi_comboBox.setGeometry(QtCore.QRect(170, 270, 141, 27))
        self.pj_visi_comboBox.setObjectName("pj_visi_comboBox")
        self.pj_visi_comboBox.addItem("")
        self.pj_visi_comboBox.addItem("")
        self.pj_visi_comboBox.addItem("")
        self.create_pjButton = QtWidgets.QPushButton(self.tab_4)
        self.create_pjButton.setGeometry(QtCore.QRect(430, 210, 101, 27))
        self.create_pjButton.setObjectName("create_pjButton")
        self.status_pj_label = QtWidgets.QLabel(self.tab_4)
        self.status_pj_label.setGeometry(QtCore.QRect(190, 470, 261, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setItalic(True)
        font.setBold(True)
        self.status_pj_label.setFont(font)
        self.status_pj_label.setStyleSheet("selection-color: rgb(115, 210, 22);")
        self.status_pj_label.setTextFormat(QtCore.Qt.PlainText)
        self.status_pj_label.setScaledContents(True)
        self.status_pj_label.setObjectName("status_pj_label")
        self.tabWidget.addTab(self.tab_4, "")
        self.tab_5 = QtWidgets.QWidget()
        self.tab_5.setObjectName("tab_5")
        self.newnameTextEdit = QtWidgets.QPlainTextEdit(self.tab_5)
        self.newnameTextEdit.setGeometry(QtCore.QRect(160, 90, 251, 31))
        self.newnameTextEdit.setObjectName("newnameTextEdit")
        self.newusernameTextEdit = QtWidgets.QPlainTextEdit(self.tab_5)
        self.newusernameTextEdit.setGeometry(QtCore.QRect(160, 130, 251, 31))
        self.newusernameTextEdit.setObjectName("newusernameTextEdit")
        self.newemailTextEdit = QtWidgets.QPlainTextEdit(self.tab_5)
        self.newemailTextEdit.setGeometry(QtCore.QRect(160, 170, 251, 31))
        self.newemailTextEdit.setObjectName("newemailTextEdit")
        self.label_32 = QtWidgets.QLabel(self.tab_5)
        self.label_32.setGeometry(QtCore.QRect(80, 100, 67, 19))
        self.label_32.setObjectName("label_32")
        self.label_33 = QtWidgets.QLabel(self.tab_5)
        self.label_33.setGeometry(QtCore.QRect(80, 140, 71, 20))
        self.label_33.setObjectName("label_33")
        self.label_34 = QtWidgets.QLabel(self.tab_5)
        self.label_34.setGeometry(QtCore.QRect(80, 180, 67, 19))
        self.label_34.setObjectName("label_34")
        self.label_35 = QtWidgets.QLabel(self.tab_5)
        self.label_35.setGeometry(QtCore.QRect(230, 50, 91, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_35.setFont(font)
        self.label_35.setObjectName("label_35")
        self.newpasswordTextEdit = QtWidgets.QPlainTextEdit(self.tab_5)
        self.newpasswordTextEdit.setGeometry(QtCore.QRect(160, 210, 251, 31))
        self.newpasswordTextEdit.setObjectName("newpasswordTextEdit")
        self.label_36 = QtWidgets.QLabel(self.tab_5)
        self.label_36.setGeometry(QtCore.QRect(80, 220, 67, 19))
        self.label_36.setObjectName("label_36")
        self.random_pwcheckBox = QtWidgets.QCheckBox(self.tab_5)
        self.random_pwcheckBox.setGeometry(QtCore.QRect(160, 250, 151, 25))
        self.random_pwcheckBox.setChecked(False)
        self.random_pwcheckBox.setObjectName("random_pwcheckBox")
        self.createuserButton = QtWidgets.QPushButton(self.tab_5)
        self.createuserButton.setGeometry(QtCore.QRect(440, 160, 88, 27))
        self.createuserButton.setObjectName("createuserButton")
        self.newuserfileTextEdit = QtWidgets.QPlainTextEdit(self.tab_5)
        self.newuserfileTextEdit.setGeometry(QtCore.QRect(160, 359, 231, 31))
        self.newuserfileTextEdit.setObjectName("newuserfileTextEdit")
        self.label_37 = QtWidgets.QLabel(self.tab_5)
        self.label_37.setGeometry(QtCore.QRect(80, 370, 67, 19))
        self.label_37.setObjectName("label_37")
        self.new_userfileToolButton = QtWidgets.QToolButton(self.tab_5)
        self.new_userfileToolButton.setGeometry(QtCore.QRect(400, 360, 27, 26))
        self.new_userfileToolButton.setObjectName("new_userfileToolButton")
        self.label_38 = QtWidgets.QLabel(self.tab_5)
        self.label_38.setGeometry(QtCore.QRect(200, 320, 181, 19))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_38.setFont(font)
        self.label_38.setObjectName("label_38")
        self.create_userfileButton = QtWidgets.QPushButton(self.tab_5)
        self.create_userfileButton.setGeometry(QtCore.QRect(440, 360, 88, 27))
        self.create_userfileButton.setObjectName("create_userfileButton")
        self.random_pw_filecheckBox = QtWidgets.QCheckBox(self.tab_5)
        self.random_pw_filecheckBox.setGeometry(QtCore.QRect(160, 400, 151, 25))
        self.random_pw_filecheckBox.setChecked(True)
        self.random_pw_filecheckBox.setObjectName("random_pw_filecheckBox")
        self.status_user_label = QtWidgets.QLabel(self.tab_5)
        self.status_user_label.setGeometry(QtCore.QRect(180, 480, 271, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setItalic(True)
        font.setBold(True)
        self.status_user_label.setFont(font)
        self.status_user_label.setStyleSheet("selection-color: rgb(115, 210, 22);")
        self.status_user_label.setTextFormat(QtCore.Qt.PlainText)
        self.status_user_label.setScaledContents(True)
        self.status_user_label.setObjectName("status_user_label")
        self.tabWidget.addTab(self.tab_5, "")
        self.verticalLayout_3.addWidget(self.tabWidget)
        mainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(mainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        mainWindow.setWindowTitle(QtWidgets.QApplication.translate("mainWindow", "GitLab Manager", None, -1))
        self.online_label.setText(QtWidgets.QApplication.translate("mainWindow", "offline", None, -1))
        self.label_15.setText(QtWidgets.QApplication.translate("mainWindow", "Login using configuration file", None, -1))
        self.label_14.setText(QtWidgets.QApplication.translate("mainWindow", "Login using URL and token", None, -1))
        self.label_18.setText(QtWidgets.QApplication.translate("mainWindow", "Configuration file", None, -1))
        self.loginButton.setText(QtWidgets.QApplication.translate("mainWindow", "Login", None, -1))
        self.loginURLTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "https://gitlab.com", None, -1))
        self.loginTokenTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "aBCfTS96G", None, -1))
        self.login_fileButton.setText(QtWidgets.QApplication.translate("mainWindow", "Login from file", None, -1))
        self.label_16.setText(QtWidgets.QApplication.translate("mainWindow", "Configuration name", None, -1))
        self.login_conffileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "python-gitlab.cfg", None, -1))
        self.status_login_label.setText(QtWidgets.QApplication.translate("mainWindow", "Select an action", None, -1))
        self.confToolButton.setText(QtWidgets.QApplication.translate("mainWindow", "...", None, -1))
        self.label_17.setText(QtWidgets.QApplication.translate("mainWindow", "Token", None, -1))
        self.label_6.setText(QtWidgets.QApplication.translate("mainWindow", "URL", None, -1))
        self.login_confnameTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "gitlab", None, -1))
        self.gen_conf_fileCheckBox.setText(QtWidgets.QApplication.translate("mainWindow", "Generate configuration file", None, -1))
        self.new_conf_fileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "python-gitlab.cfg", None, -1))
        self.new_conf_file_label.setText(QtWidgets.QApplication.translate("mainWindow", "Configuration file", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_1), QtWidgets.QApplication.translate("mainWindow", "Login", None, -1))
        self.groupsgTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "Group1", None, -1))
        self.downpjButton.setText(QtWidgets.QApplication.translate("mainWindow", "Download projects", None, -1))
        self.pjdownTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "project1", None, -1))
        self.UsersgfiletoolButton.setText(QtWidgets.QApplication.translate("mainWindow", "...", None, -1))
        self.grouppjTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "Group1", None, -1))
        self.label_39.setText(QtWidgets.QApplication.translate("mainWindow", "Group name", None, -1))
        self.sgTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "G1_", None, -1))
        self.label_21.setText(QtWidgets.QApplication.translate("mainWindow", "Group Name", None, -1))
        self.label_22.setText(QtWidgets.QApplication.translate("mainWindow", "Create subgroups from users file", None, -1))
        self.create_sgButton.setText(QtWidgets.QApplication.translate("mainWindow", "Create subgroups", None, -1))
        self.label_41.setText(QtWidgets.QApplication.translate("mainWindow", "Project name", None, -1))
        self.label_19.setText(QtWidgets.QApplication.translate("mainWindow", "SubGroup Prefix", None, -1))
        self.label_40.setText(QtWidgets.QApplication.translate("mainWindow", "Download projects from subgroups", None, -1))
        self.status_tools_label.setText(QtWidgets.QApplication.translate("mainWindow", "Select an action", None, -1))
        self.label_20.setText(QtWidgets.QApplication.translate("mainWindow", "Users file", None, -1))
        self.UsersgfileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "users.csv", None, -1))
        self.downgroupTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "Group1", None, -1))
        self.label_7.setText(QtWidgets.QApplication.translate("mainWindow", "Group name", None, -1))
        self.label_8.setText(QtWidgets.QApplication.translate("mainWindow", "Download projects from group", None, -1))
        self.downgrouppushButton.setText(QtWidgets.QApplication.translate("mainWindow", "Download projects", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QtWidgets.QApplication.translate("mainWindow", "Tools", None, -1))
        self.group_patternTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "group1_.*", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("mainWindow", "Group pattern", None, -1))
        self.newgroupfileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "groups.txt", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("mainWindow", "Filename", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("mainWindow", "Generate groups file from pattern", None, -1))
        self.gen_groupsfileButton.setText(QtWidgets.QApplication.translate("mainWindow", "Gen groups file", None, -1))
        self.groupfileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "groups.txt", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("mainWindow", "Remove groups from groups file", None, -1))
        self.label_5.setText(QtWidgets.QApplication.translate("mainWindow", "Groups file", None, -1))
        self.remove_groupsButton.setText(QtWidgets.QApplication.translate("mainWindow", "Remove groups", None, -1))
        self.status_groups_label.setText(QtWidgets.QApplication.translate("mainWindow", "Select an action", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QtWidgets.QApplication.translate("mainWindow", "Groups Admin", None, -1))
        self.label_11.setText(QtWidgets.QApplication.translate("mainWindow", "Projects file", None, -1))
        self.label_13.setText(QtWidgets.QApplication.translate("mainWindow", "Create projects from file", None, -1))
        self.create_pjfileButton.setText(QtWidgets.QApplication.translate("mainWindow", "Create projects", None, -1))
        self.pjfiletoolButton.setText(QtWidgets.QApplication.translate("mainWindow", "...", None, -1))
        self.pjfileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "project_list.csv", None, -1))
        self.pjnameTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "project1", None, -1))
        self.pjusernameTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "user1", None, -1))
        self.label_28.setText(QtWidgets.QApplication.translate("mainWindow", "Create project to other user", None, -1))
        self.label_29.setText(QtWidgets.QApplication.translate("mainWindow", "Project name", None, -1))
        self.label_30.setText(QtWidgets.QApplication.translate("mainWindow", "Username", None, -1))
        self.label_31.setText(QtWidgets.QApplication.translate("mainWindow", "Visibility", None, -1))
        self.pj_visi_comboBox.setItemText(0, QtWidgets.QApplication.translate("mainWindow", "Public", None, -1))
        self.pj_visi_comboBox.setItemText(1, QtWidgets.QApplication.translate("mainWindow", "Private", None, -1))
        self.pj_visi_comboBox.setItemText(2, QtWidgets.QApplication.translate("mainWindow", "Internal", None, -1))
        self.create_pjButton.setText(QtWidgets.QApplication.translate("mainWindow", "Create project", None, -1))
        self.status_pj_label.setText(QtWidgets.QApplication.translate("mainWindow", "Select an action", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), QtWidgets.QApplication.translate("mainWindow", "Project Admin", None, -1))
        self.newnameTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "Test User", None, -1))
        self.newusernameTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "test", None, -1))
        self.newemailTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "test@email.com", None, -1))
        self.label_32.setText(QtWidgets.QApplication.translate("mainWindow", "Name", None, -1))
        self.label_33.setText(QtWidgets.QApplication.translate("mainWindow", "Username", None, -1))
        self.label_34.setText(QtWidgets.QApplication.translate("mainWindow", "Email", None, -1))
        self.label_35.setText(QtWidgets.QApplication.translate("mainWindow", "Create user", None, -1))
        self.label_36.setText(QtWidgets.QApplication.translate("mainWindow", "Password", None, -1))
        self.random_pwcheckBox.setText(QtWidgets.QApplication.translate("mainWindow", "Random password", None, -1))
        self.createuserButton.setText(QtWidgets.QApplication.translate("mainWindow", "Create user", None, -1))
        self.newuserfileTextEdit.setPlainText(QtWidgets.QApplication.translate("mainWindow", "users.csv", None, -1))
        self.label_37.setText(QtWidgets.QApplication.translate("mainWindow", "Users file", None, -1))
        self.new_userfileToolButton.setText(QtWidgets.QApplication.translate("mainWindow", "...", None, -1))
        self.label_38.setText(QtWidgets.QApplication.translate("mainWindow", "Create users from file", None, -1))
        self.create_userfileButton.setText(QtWidgets.QApplication.translate("mainWindow", "Create users", None, -1))
        self.random_pw_filecheckBox.setText(QtWidgets.QApplication.translate("mainWindow", "Random password", None, -1))
        self.status_user_label.setText(QtWidgets.QApplication.translate("mainWindow", "Select an action", None, -1))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5), QtWidgets.QApplication.translate("mainWindow", "User Admin", None, -1))
        self._enable_tabs()

    def __get_relative_path(self, path):
        dir = QtCore.QDir(".")
        relative_path = dir.relativeFilePath(path)
        
        return relative_path

    def _selectConfFile(self):        
        conf_filename = QtWidgets.QFileDialog(self.centralwidget).getOpenFileName()[0]
        relative_path = self.__get_relative_path(conf_filename)        
        self.login_conffileTextEdit.setPlainText(relative_path)
        
    def _selectUsersgFile(self):
        user_filename = QtWidgets.QFileDialog(self.centralwidget).getOpenFileName()[0]
        relative_path = self.__get_relative_path(user_filename)
        self.UsersgfileTextEdit.setPlainText(relative_path)

    def _selectProjectsFile(self):
        pj_filename = QtWidgets.QFileDialog(self.centralwidget).getOpenFileName()[0]
        relative_path = self.__get_relative_path(pj_filename)
        
        self.pjfileTextEdit.setPlainText(relative_path)

    def _select_newusers_file(self):
        users_filename = QtWidgets.QFileDialog(self.centralwidget).getOpenFileName()[0]
        relative_path = self.__get_relative_path(users_filename)
        
        self.newuserfileTextEdit.setPlainText(relative_path)


    def __show_online(self):
        user_type = {True: "(admin)", False: "(normal)"}
        
        if self.GitLab:
            self.status_login_label.setText("login successful")
            user = self.GitLab.get_user()
            username = user.get_username()
            is_admin = user.is_admin()
            
            self.online_label.setText(f"{username} online {user_type[is_admin]}")
            
        else:
            self.status_login_label.setText("login failed")
            self.online_label.setText("offline")

    def _enable_tabs(self):
        if self.GitLab:
            is_admin = self.GitLab.get_user().is_admin()
            
            if not is_admin:
                self.tabWidget.setTabEnabled(1, True)
                self.tabWidget.setTabEnabled(2, False)
                self.tabWidget.setTabEnabled(3, False)
                self.tabWidget.setTabEnabled(4, False)
            else:
                self.tabWidget.setTabEnabled(1, True)
                self.tabWidget.setTabEnabled(2, True)
                self.tabWidget.setTabEnabled(3, True)
                self.tabWidget.setTabEnabled(4, True)
        else:
            self.tabWidget.setTabEnabled(1, False)
            self.tabWidget.setTabEnabled(2, False)
            self.tabWidget.setTabEnabled(3, False)
            self.tabWidget.setTabEnabled(4, False)

    def _startLoginFile(self):
        filename = self.login_conffileTextEdit.toPlainText()
        configname = self.login_confnameTextEdit.toPlainText()
        self.status_login_label.setText("logging from file... wait")

        self.GitLab = GitLabLogin().login_from_file(configname, filename)
                
        self._enable_tabs()
        self.__show_online()
    
    def _show_conffile(self):
        if self.gen_conf_fileCheckBox.isChecked():
            self.new_conf_fileTextEdit.setDisabled(False)
            self.new_conf_file_label.setDisabled(False)
        else:
            self.new_conf_fileTextEdit.setDisabled(True)
            self.new_conf_file_label.setDisabled(True)
    
    def _startLogin(self):
        url = self.loginURLTextEdit.toPlainText()
        token = self.loginTokenTextEdit.toPlainText()
        self.status_login_label.setText("logging from token... wait")
        
        self.GitLab = GitLabLogin().login(url, token)
        
        self._enable_tabs()
        self.__show_online()
        
        if self.gen_conf_fileCheckBox.isChecked() and self.GitLab:
            filename = self.new_conf_fileTextEdit.toPlainText()
            GitLabLogin().gen_config_file(url, token, filename)
    
    
    def _create_subgroup(self):
        group = self.groupsgTextEdit.toPlainText()
        sg_prefix = self.sgTextEdit.toPlainText()
        users_file = self.UsersgfileTextEdit.toPlainText()

        GMTools = self.GitLab.get_tools()
        self.status_tools_label.setText("creating subgroups... wait")
        
        sg_created, status = GMTools.add_users_subgroup_from_file(users_file, group, sg_prefix)
        
        if sg_created and status:
            self.status_tools_label.setText("subgroups created succesfully")
        elif sg_created and not status:
            self.status_tools_label.setText("subgroups created with errors")
        else:
            self.status_tools_label.setText("subgroups creation failed")
            
    def _create_projects_from_file(self):
        projects_file = self.pjfileTextEdit.toPlainText()
        
        GMTools = self.GitLab.get_tools()
        self.status_pj_label.setText("creating projects... wait")
        
        pj_created = GMTools.create_projects_from_file(projects_file)
    
        if pj_created:
            self.status_pj_label.setText("projects created succesfully")
        else:
            self.status_pj_label.setText("projects creation failed")
                
    def _create_project_to_user(self):
        pj_obj = None
        
        project_name = self.pjnameTextEdit.toPlainText()
        username = self.pjusernameTextEdit.toPlainText()
        _visibility = str(self.pj_visi_comboBox.currentText()).lower()
        
        explorer = self.GitLab.get_explorer()
        user = explorer.find_user(username)
        
        if user:
            pj_data = ProjectData(name=project_name, visibility=_visibility)
            pj_obj = user.create_project(pj_data)
            
            if pj_obj:
                self.status_pj_label.setText("project created successfully")
        
        if not pj_obj:
            self.status_pj_label.setText("project creation failed")
                
    def _download_sg_projects(self):
        self.status_tools_label.setText("downloading projects... wait")
        group_name = self.grouppjTextEdit.toPlainText()
        project_name = self.pjdownTextEdit.toPlainText()
        
        GMTools = self.GitLab.get_tools()
        download = GMTools.download_projects_by_subgroups(group_name, project_name)
        
        if download:
            self.status_tools_label.setText("download successful")
        else:
            self.status_tools_label.setText("download finished with errors")

    def _download_group_projects(self):
        group_name = self.downgroupTextEdit.toPlainText()
        
        GMTools = self.GitLab.get_tools()        
        download = GMTools.download_all_from_group(group_name)
        self.status_tools_label.setText("downloading projects. wait...")
        
        if download:
            self.status_tools_label.setText("download successful")
        else:
            self.status_tools_label.setText("download finished with errors")
            

    def _disable_password(self):
        if self.random_pwcheckBox.isChecked():
            self.newpasswordTextEdit.setDisabled(True)
        else:
            self.newpasswordTextEdit.setDisabled(False)

    def _gen_groupsfile(self):
        group_pattern = self.group_patternTextEdit.toPlainText()
        filename = self.newgroupfileTextEdit.toPlainText()
        
        GMTools = self.GitLab.get_tools()
        gen_file = GMTools.gen_groupfile_from_pattern(filename, group_pattern)
        
        if gen_file:
            self.status_groups_label.setText("groupfile created successfully")
        else:
            self.status_groups_label.setText("groupfile creation failed")

    def _remove_groups(self):
        group_file = self.groupfileTextEdit.toPlainText()
        
        GMTools = self.GitLab.get_tools()
        remove_groups = GMTools.remove_groups_from_file(group_file)
        
        if remove_groups:
            self.status_groups_label.setText("groups removed successfully")
        else:
            self.status_groups_label.setText("groups removing failed")

    def _create_user(self):
        _name = self.newnameTextEdit.toPlainText()
        _username = self.newusernameTextEdit.toPlainText()
        _email = self.newemailTextEdit.toPlainText()
        _password = self.newpasswordTextEdit.toPlainText()
        
        if self.random_pwcheckBox.isChecked():
            user_data = UserData(name=_name, username=_username, email=_email)
            self.newpasswordTextEdit.setDisabled(True)
        else:
            user_data = UserData(name=_name, username=_username, email=_email, password=_password)
            
        user_created = self.GitLab.create_user(user_data)
        
        if user_created:
            self.status_user_label.setText("user created successfully")
        else:
            self.status_user_label.setText("user creation failed")

    
    def _create_users_from_file(self):
        users_file = self.newuserfileTextEdit.toPlainText()
        
        GMTools = self.GitLab.get_tools()
        users_created = GMTools.activate_users_from_file(users_file)
        
        if users_created:
            self.status_user_label.setText("users created successfully")
        else:
            self.status_user_label.setText("users creation failed")

    def initUI(self):
        #login from file
        self.confToolButton.clicked.connect(self._selectConfFile)
        self.login_fileButton.clicked.connect(self._startLoginFile)
        
        #login from URL and token
        self.loginButton.clicked.connect(self._startLogin)
        self.gen_conf_fileCheckBox.clicked.connect(self._show_conffile)
        
        #create subgroups from file
        self.create_sgButton.clicked.connect(self._create_subgroup)
        self.UsersgfiletoolButton.clicked.connect(self._selectUsersgFile)
        
        #download projects from subgroups
        self.downpjButton.clicked.connect(self._download_sg_projects)
        
        #download projects from group
        self.downgrouppushButton.clicked.connect(self._download_group_projects)
        
        #generate groups file from pattern
        self.gen_groupsfileButton.clicked.connect(self._gen_groupsfile)
        
        #remove groups from groupfile
        self.remove_groupsButton.clicked.connect(self._remove_groups)
        
        #create projects from file
        self.pjfiletoolButton.clicked.connect(self._selectProjectsFile)
        self.create_pjfileButton.clicked.connect(self._create_projects_from_file)
    
        #create project to user
        self.create_pjButton.clicked.connect(self._create_project_to_user)
        
        #create user
        self.createuserButton.clicked.connect(self._create_user)
        self.random_pwcheckBox.clicked.connect(self._disable_password)
        
        #create users from file
        self.new_userfileToolButton.clicked.connect(self._select_newusers_file)
        self.create_userfileButton.clicked.connect(self._create_users_from_file)
        


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = QtWidgets.QMainWindow()
    ui = Ui_mainWindow()
    ui.setupUi(mainWindow)
    ui.initUI()
    mainWindow.show()
    sys.exit(app.exec_())

